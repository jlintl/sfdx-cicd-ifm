if [ -z "$1" ]
  then
    echo "No argument for a scratch org alias supplied. Recommended format is <initials of developer>.<User story id>"
    exit 1
fi

sfdx force:org:create -s -f config/project-scratch-def.json -a $1
sfdx force:source:push
sfdx force:data:tree:import -p ./data/plan.json
#sfdx force:user:create --setalias sysAdmin -f config/SysAdmin.json
#sfdx force:user:create --setalias sales --definitionfile config/Sales.json
#sfdx force:user:create --setalias service --definitionfile config/Service.json
#sfdx force:user:create --setalias int --definitionfile config/Integration.json
#sfdx force:user:password:generate -u sysAdmin
#sfdx force:user:display -u sysAdmin
sfdx force:org:display
sfdx force:org:open -p /lightning/setup/SetupOneHome/home