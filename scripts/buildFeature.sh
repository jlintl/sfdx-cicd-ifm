if [ -z "$1" ]
  then
    echo "No argument for a scratch org alias supplied. Recommended format is <initials of developer>.<User story id>"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No argument for a name for the new created branch. Please enter a name for the new branch as a second parameter. Recommended format is <Sprint #>.<User story id>"
    exit 1
fi

if [ -z "$3" ]
  then
    ${3:-"release/release_1.0"}
    echo "WARNING: No argument for a name for the branch which should be the basis for the newly created branch. If not set manually, default value='release/release_1.0' is set."
fi

git checkout -b $2 $3
sfdx force:org:create -s -f config/project-scratch-def.json -a $1
sfdx force:source:push
sfdx force:data:tree:import -p ./data/plan.json
#sfdx force:user:create --setalias sysAdmin -f config/SysAdmin.json
#sfdx force:user:create --setalias sales --definitionfile config/Sales.json
#sfdx force:user:create --setalias service --definitionfile config/Service.json
#sfdx force:user:create --setalias int --definitionfile config/Integration.json
#sfdx force:user:password:generate -u sysAdmin
#sfdx force:user:display -u sysAdmin
sfdx force:org:display
sfdx force:org:open -p /lightning/setup/SetupOneHome/home