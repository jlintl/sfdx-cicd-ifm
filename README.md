# Bitbucket CI/CD for Salesforce

This project contains a fully configured CI/CD pipeline that works with Salesforce DX projects following the org development model from https://trailhead.salesforce.com/content/learn/modules/org-development-model. You can include a copy of, or import, this project's .yml file in your own project. This Bitbucket Pipeline is an adapted version from https://github.com/forcedotcom/sfdx-bitbucket-org and https://github.com/forcedotcom/sfdx-bitbucket-package, so that it can be used for CI tasks when using the [Git FLow](https://nvie.com/posts/a-successful-git-branching-model/) branching model on a Salesforce implementation project with DX sources.

# What It Does

1. Control the code quality with Apex PMD on each commit
2. Validate the feature in a extra sandbox (CI) which is a clone of prod metadata
3. Deploy and run tests in a new scratch org (currently not in scope => commented out in the yml file)
4. When merging into the `develop` branch,deploy into a QA sandbox (functional feature tests)
5. Deploy and run all apex tests in the INTEG sandbox
6. When merging into a `release/` branch, deploy to the STAGE sandbox (release branch will be created from the develop branch by the Release Manager)
7. When merging into the `master` branch, deploy to PROD



The steps bellow describe how to set up a pipeline for this developement and release management process:

![overview](Images/overview.png)

### Note - We recommend using scratch orgs only in case you are familiar with the current restrictions of the scratch orgs! 

### Use scratch orgs to test your code only:

 * your new feature has no or minor dependancies with other metadata like objects, fields and can be tested in a empty salesforce environment - mostly used for apex classes trigger testing, vf pages together with lightning components!

### Do NOT use scratch orgs in this circumstances:

* your new feature should be tested with remote systems
* your new feature should be tested together with a bunch of metadata like managed packages instaleld in your production org
* your new feature requires a lot of manual configuration steps to be done on the scratch org before a proper testing can take place

In order to use scratch orgs in those cases you will be involved in a writing some automation scripting and this will be more error prone which can even slow your DevOps process and instead of improving it.

### Recoomendation: once the dev team is mature enough and familiar with the metadata and the Apps in their orgs, they can establish a generic way to use use scratch orgs with the required metadata!

## Step 0 - Prerequisites:

* On your local machine, make sure you have the VSCode with sfdx extension and Salesforce CLI installed. Check by running `sfdx force --help` and confirm you see the command output. If you don't have it installed you can download and install it from https://developer.salesforce.com/tools/sfdxcli

## Step 1 - Create the certificate and the encrypted private key:

* We will use a script to generate the certificates to use with JWT-based auth, so make sure you have the ability to execute shell script.

* Give the right to execute the bootstrap-jwt-auth script
```
$ chmod u+x scripts/bootstrap-jwt-auth-prerequisites.sh
```
* To generate the certificate and the encrypted private key, execute the script. Make sure to use a strong password (i.e. long, unique, and randomly-generated).
```
$ ./scripts/bootstrap-jwt-auth-prerequisites.sh <password> <env>
```
Example: `.scripts//bootstrap-jwt-auth-prerequisites.sh put_here_your_strong_password PROD`

* Set your PROD password in a **protected** Bitbucket Pipelines environment variable named `PROD_KEY_PASSWORD` using the Bitbucket Pipelines UI (under `Settings > Repository variables`).

Note: This command will generate 2 folders: certificates and assets. You will need the certificate .crt file generated in the certificate folder for the next step. The asset file will be used for the authorization against an ORG. The certificate folder will exist only on your local git. Therefore it won't be uploaded to the git (see excluded folder defined in .git ignore)!


## Step 2 - Create connected app to authenticate with JWT-Based Flow:

* Authorize an ORG from your project.

* Create a connected app to use the JWT-Based Flow to login (https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_connected_app.htm). For more info on setting up JWT-based auth, see also the Salesforce DX Developer Guide (https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev)

* Do not forget to upload you crt file generated in the previous step to your connected app (see steps 7 and 8 from the documentation)

* From this JWT-based connected app on Salesforce, retrieve the generated `Consumer Key` from your org.

* Clear the terminal history: `history -c && clear`

* Set your Consumer Key in a Bitbucket Pipelines environment variable named `PROD_CONSUMERKEY` using the Bitbucket Pipelines UI (under `Settings > Repository variables`).

* Set your Username in a Bitbucket Pipelines environment variable named `PROD_USERNAME` using the Bitbucket Pipelines UI (use an API only user).

* Commit the `PROD_server.key.enc` file from the `build` folder and store it into a shared document library of your repo. The `certificate` folder (containing the .crt file you uploaded in Salesforce) can be deleted.
```
$ git add assets/PROD_server.key.enc
$ git commit -m 'add PROD env encrypted server key'
$ git push
```

## Step 3 - Repeat from step 1 & 2 for each environment you need to connect to!

    * PROD
    * UAT
    * INTEG
    * QA
    
## Step 4 - Configure Apex PMD:

PMD is source code analyzer which can help the developers to check their code quality based on predifined ruleset. See the docomentation here: https://pmd.github.io/latest/pmd_rules_apex.html

* Add to your repo a custom-apex-rules.xml ruleset file for Apex PMD. A sample ruleset can be found in `custom-apex-rules.xml`

* Create a Bitbucket Pipelines environment variable named `PMD_VERSION` to specify the PMD version to use (such as `6.19.0`). PMD releases are listed here: https://github.com/pmd/pmd/releases

* Create a Bitbucket Pipelines environment variable named `PMD_MINIMUM_PRIORITY` to trigger a build failure when a high priority issue is found (recommended error threshold: 2)
    
## Step 5 - Create and commit the yml file:
* Create or re-use a docker image with Salesforce CLI installed, such as:  https://hub.docker.com/r/mehdisfdc/sfdx-cli/dockerfile

* Create or re-use the `bitbucket-pipelines.yml file`

## Step 6 - Develop and Configure against your dev org

Due to the fact that the establishing of a successfull DevOps within an organaization might be very challenging and it will be continiously improved based on the exeperience within the team. Therefore it is recommended to start with small steps. The transition to the full automation: retreiving all metadata and configurations with tools like Vscode or even with CLI commands  should be done step by step. 

Some admins or even devs still prefer to collect all their configuration changes in a change set or even in a unmanaged package. Understand the diffrenece between change set and unmanaged package!
* https://help.salesforce.com/articleView?id=changesets.htm&type=5
* https://help.salesforce.com/articleView?id=sharing_apps.htm&type=5


Therefore we might need an easy way to convert this set/ unmanaged package into our current project with one step as our source of truth is our git repository now:

* We will use a script to convert the change set/ unmanaged package created in your dev org.

* Give the right to execute the convertUnmanaged.sh
```
$ chmod u+x scripts/convertUnmanaged.sh
```
* To convert your ChangeSet/UnmanagedPackage, execute the script
```
$ ./scripts/convertUnmanaged.sh <orgName> <UnmanagedPackageName>
```
Example: `.scripts//convertUnmanaged.sh exampleOrg DemoPackage`

## Step 7 - Activate pieplines in your org

Enable pieplines under `Pipelines > Settings > Enable Pipelines`)

On the next commit to your feature branch the pipeline will be triggered!

