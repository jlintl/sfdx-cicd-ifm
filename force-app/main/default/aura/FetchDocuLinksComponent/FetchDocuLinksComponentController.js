({

    doInit: function (component, event, helper) {

        // var actions = [
        //     { label: 'View', name: 'View' }
        // ];

        component.set('v.columns', [
            { label: 'Id', fieldName: 'docAccessLink', type: 'url', sortable: 'true',typeAttributes: {label: { fieldName: 'docId' }, target: '_blank'}},
            { label: 'Title', fieldName: 'reference', type: 'text', sortable: 'true' },
            { label: 'Type', fieldName: 'docType', type: 'text', sortable: 'true' },
            { label: 'Sales Organization', fieldName: 'companyNo', type: 'text', sortable: 'true' },
            { label: 'Output Channel', fieldName: 'outputChannel', type: 'text', sortable: 'true' },
            { label: 'Creation Date', fieldName: 'creationDate', type: 'text', sortable: 'true' }
            //,{ label: 'action', fieldName: 'action', type: 'action', typeAttributes: { rowActions: actions } }

        ]);
    },
    fetchDocuments : function(component, event, helper) {
        helper.fetchDocuLinks(component,event,helper);
    }
})