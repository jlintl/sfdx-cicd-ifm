({
    fetchDocuLinks : function(component,event,helper) {
        var action = component.get('c.fetchDocuLinks');
        action.setParams({ accId: component.get("v.recordId"),
                           docType: component.find("docType").get("v.value"),
                           sizeLimit: component.find("pageSize").get("v.value")
    });
        // Set up the callback
        var self = this;

        action.setCallback(this, function (response) {
            //console.log('accId: '+component.get("v.recordId"));
            //console.log('docTyp: '+component.find("docType").get("v.value"));
            //console.log('sizeLimit: '+component.find("pageSize").get("v.value"));
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('List received '+storeResponse);
                // Check if response is empty
                if(!Array.isArray(storeResponse) || !storeResponse.length){
                    // if the list is empty
                    console.log("Warning");
                    // Show user on the UI that there is an error
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                         "title": "Warning!",
                         "duration": "5000",
                         "mode": "dismissible",
                         "type": "warning",
                         "message": "There are no documents for this type of document!"
                     });
                     component.set("v.docuList", null);
                     toastEvent.fire();
                }else{
                    console.log('SUCCESS');
                    console.log(JSON.stringify(storeResponse));
                    component.set("v.docuList", storeResponse);
                }
            } else if(state === "ERROR"){
                console.log('ERROR');
                // Show user on the UI that there is an error
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                     "title": "Error!",
                     "duration": "5000",
                     "mode": "dismissible",
                     "type": "error",
                     "message": "Document Links could not be loaded. There was a technical error"
                 });
                 toastEvent.fire();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    },
})