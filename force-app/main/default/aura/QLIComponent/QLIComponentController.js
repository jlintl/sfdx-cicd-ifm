({
    doInit: function (component, event, helper) {

        var actions = [
            { label: 'View', name: 'View' },
            { label: 'Edit', name: 'Edit' },
            { label: 'Delete', name: 'Delete' }
        ];

        component.set('v.columns', [
            //{ label: 'Id', fieldName: 'Id', type: 'text', sortable: 'true' },
            //{ label: 'Account Name', fieldName: 'AccountName', type: 'text', sortable: 'true' },
            { label: 'Quantity', fieldName: 'Quantity', type: 'number', sortable: 'true' },
            { label: 'Product Code', fieldName: 'Product_Code__c', type: 'text', sortable: 'true' },
            { label: 'Status', fieldName: 'Status__c', type: 'text', sortable: 'true' },
            { label: 'Available Stock', fieldName: 'Available_Stock__c', type: 'number', sortable: 'true' },
            // { label: 'Currency', fieldName: 'Currency__c', type: 'text', sortable: 'true' },
            { label: 'Customer-specific Price', fieldName: 'Customer_specific_price__c', type: 'currency', sortable: 'true' },
            { label: 'Delivery Date', fieldName: 'Delivery_Date__c', type: 'date', sortable: 'true' },
            { label: 'Last Modified Date', fieldName: 'LastModifiedDate', type: 'date', sortable: 'true' },
            { label: 'action', fieldName: 'action', type: 'action', typeAttributes: { rowActions: actions } }

        ]);
        // Fetch the quote line item list from the Apex controller
        helper.getQLIs(component);
        setInterval(function () { helper.getQLIs(component); }, component.get("v.pollInterval"));
    },
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        console.log('rowId ' + row.Id);

        switch (action.name) {
            case 'View':
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": 'https://ifmpoc.lightning.force.com/lightning/r/QuoteLineItem/' + row.Id + '/view'
                });
                urlEvent.fire();
                break;
            case 'Edit':
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": 'https://ifmpoc.lightning.force.com/lightning/r/QuoteLineItem/' + row.Id + '/edit'
                });
                urlEvent.fire();
                break;
            case 'Delete':
                helper.deleteQLI(component,row);
                break;
        }
    },
        //Method gets called by onsort action,
        handleSort : function(component,event,helper){
            //Returns the field which has to be sorted
            var sortBy = event.getParam("fieldName");
            //returns the direction of sorting like asc or desc
            var sortDirection = event.getParam("sortDirection");
            //Set the sortBy and SortDirection attributes
            component.set("v.sortBy",sortBy);
            component.set("v.sortDirection",sortDirection);
            // call sortData helper function
            helper.sortData(component,sortBy,sortDirection);
        }
})