({
    // Fetch the quote line items from the Apex controller
    getQLIs: function (component) {
        var action = component.get('c.getQLIs');
        action.setParams({ quoteId: component.get("v.recordId") });
        // Set up the callback
        var self = this;
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var rows = response.getReturnValue();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.Quote.Account.Name) {
                        row.AccountName = row.Quote.Account.Name;
                    }
                }
                component.set('v.quoteLineItems', rows);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    },
    deleteQLI: function (component,row) {
        console.log('row' + row);
        var action = component.get('c.deleteQLI');
        action.setParams({ qli: row });
        // Set up the callback
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('SUCCESS');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                     "title": "Success!",
                     "type": "success",
                     "message": "Quote Line Item was deleted."
                 });
                 toastEvent.fire();
            } else if (state === "ERROR") {
                console.log('ERROR');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                     "title": "Error!",
                     "type": "error",
                     "message": "Quote Line Item could not be deleted."
                 });
                 toastEvent.fire();
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    },
    
    sortData : function(component,fieldName,sortDirection){
        var data = component.get("v.quoteLineItems");
        //function to return the value stored in the field
        var key = function(a) { return a[fieldName]; }
        var reverse = sortDirection == 'asc' ? 1: -1;
        
        // to handel number/currency type fields 
        if(fieldName == 'NumberOfEmployees'){ 
            data.sort(function(a,b){
                var a = key(a) ? key(a) : '';
                var b = key(b) ? key(b) : '';
                return reverse * ((a>b) - (b>a));
            }); 
        }
        else{// to handel text type fields 
            data.sort(function(a,b){ 
                var a = key(a) ? key(a).toLowerCase() : '';//To handle null values , uppercase records during sorting
                var b = key(b) ? key(b).toLowerCase() : '';
                return reverse * ((a>b) - (b>a));
            });    
        }
        //set sorted data to accountData attribute
        component.set("v.quoteLineItems",data);
    }
})