public with sharing class QLIComponentController {

    @AuraEnabled
    public static List<QuoteLineItem> getQLIs(Id quoteId){
        return [SELECT Id,Quote.Account.Name,Quantity,Delivery_Date__c,LastModifiedDate, Product_Code__c, Status__c, Available_Stock__c, Currency__c, Customer_specific_price__c FROM QuoteLineItem WHERE QuoteId =: quoteId];
    }

    @AuraEnabled
    public static void deleteQLI(QuoteLineItem qli){
            Database.delete(qli,true);
    }
}