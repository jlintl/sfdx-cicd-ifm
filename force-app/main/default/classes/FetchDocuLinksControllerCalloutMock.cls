global with sharing class FetchDocuLinksControllerCalloutMock  implements HttpCalloutMock{
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Set mock for callout
        Test.setMock(HttpCalloutMock.class, new FetchDocuLinksControllerCalloutMock());
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('[{"docId":"100316085","docType": "ORDER_RESPONSE","companyNo": 5010,"ifmCustomer": "5046996","docAccessLink": "http://deeslx81.es.de.ifm:8822/docArchiveAccess?docId=3b14900d-f062-43dd-8f7e-85016ff9fc6c","outputChannel": "sendback","creationDate": "2014-06-24 17:15:13.0","reference": "Test 1"}'+
        ', { "docId": "100316085", "docType": "ORDER_RESPONSE", "companyNo": 5010, "ifmCustomer": "5046996", "docAccessLink": "http://deeslx81.es.de.ifm:8822/docArchiveAccess?docId=32ae4622-eac8-4ca6-8b96-dd52137a2b17", "outputChannel": "sendback", "creationDate": "2014-07-04 09:37:30.0", "reference": "Test 1" }]');
        response.setStatusCode(200);
        return response; 
    }
}