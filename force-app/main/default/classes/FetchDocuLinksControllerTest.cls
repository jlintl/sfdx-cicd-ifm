@IsTest
public with sharing class FetchDocuLinksControllerTest {

@IsTest
public static void testFetchDocuLinks(){
        // Create test data
        Account acc = new Account();
        acc.Name = 'Account';
        acc.Default_ERP_Number__c = '001234';
        insert acc;
    // Set mock for callout
    Test.setMock(HttpCalloutMock.class, new FetchDocuLinksControllerCalloutMock());
    Test.startTest();
    List<FetchDocuLinksController.DocuLink> unitToBeTested = FetchDocuLinksController.fetchDocuLinks(acc.Id,'ORDER_RESPONSE',100);
    Test.stopTest();
    System.debug('unitToBeTested: '+unitToBeTested);
    System.assertEquals(2,unitToBeTested.size());
    System.assertEquals('100316085', unitToBeTested.get(0).docId);
    System.assertEquals('5010', unitToBeTested.get(0).companyNo);
    System.assertEquals('http://deeslx81.es.de.ifm:8822/docArchiveAccess?docId=3b14900d-f062-43dd-8f7e-85016ff9fc6c', unitToBeTested.get(0).docAccessLink);
    System.assertEquals('100316085', unitToBeTested.get(1).docId);
    System.assertEquals('5010', unitToBeTested.get(1).companyNo);
    System.assertEquals('http://deeslx81.es.de.ifm:8822/docArchiveAccess?docId=32ae4622-eac8-4ca6-8b96-dd52137a2b17', unitToBeTested.get(1).docAccessLink);
}
}