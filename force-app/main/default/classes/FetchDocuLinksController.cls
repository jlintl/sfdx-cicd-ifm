public with sharing class FetchDocuLinksController {

    public static String endpoint = 'https://pocsalesforce.ifm.com/api/v1/sfdc-customerdocs-experience/getDocuments?queryString=';
    
    //test change
    @AuraEnabled
    public static List<DocuLink> fetchDocuLinks(Id accId,String docType,Integer sizeLimit){
        
        Account acc = new Account();

        // Query account information
        try{
           acc = [SELECT Id,Default_ERP_Number__c,Default_Sales_Organization_Id__c FROM Account WHERE Id =: accId LIMIT 1];
        }catch(QueryException e){
        }
        List<DocuLink> docList = new List<DocuLink>();
        Http h = new Http();
       // Create callout request
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('GET');

      // Build up URL
        endpoint += 'customerNo%3D'+acc.Default_ERP_Number__c.replaceFirst('^0+','');
        endpoint += '%26companyNo%3D'+acc.Default_Sales_Organization_Id__c;
        if(docType != null){
          endpoint += '%26docType%3D'+docType.toUpperCase();
        }
        if(sizeLimit != null){
          endpoint += '%26limit%3D'+sizeLimit;
        }else{
          endpoint += '%26limit%3D100';
        }
        //endpoint += '&orderBy='+sizeLimit;
        System.debug('endpoint URL: '+endpoint);
        req.setEndpoint(endpoint); 
        HttpResponse response = h.send(req);
       //  System.debug('Response '+response);
        String body = response.getBody();
        System.debug('Response.body '+body);
        docList = (List<DocuLink>) JSON.deserialize(body, List<DocuLink>.class); 
        return docList;
    }   

    public class DocuLink{
        @AuraEnabled public String docId;
        @AuraEnabled public String docType;
        @AuraEnabled public String companyNo;
        @AuraEnabled public String docAccessLink;
        @AuraEnabled public String outputChannel;
        @AuraEnabled public String creationDate;
        @AuraEnabled public String reference;
        
    }
}